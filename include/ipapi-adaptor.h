#ifndef IPAPI_ADAPTOR_H_INCLUDED
#define IPAPI_ADAPTOR_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "co.ipapi.h"

extern const string IpapiPath;

class IpapiAdaptor
: public DBusAdaptor,
  public co::ipapi_adaptor
{
public:
    IpapiAdaptor(DBus::Connection &connection);
    virtual ~IpapiAdaptor() {}

    bool update_values();

    void properties_changed(const std::vector<std::string>& properties);
};

#endif /* IPAPI_ADAPTOR_H_INCLUDED */
