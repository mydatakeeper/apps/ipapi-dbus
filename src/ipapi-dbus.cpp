#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <thread>
#include <csignal>

#include <json/json.hh>

#include <dbus-c++/dbus.h>

#include <mydatakeeper/arguments.h>

#include "ipapi-adaptor.h"

const string IpapiName("co.ipapi");

using namespace std;

DBus::BusDispatcher dispatcher;

condition_variable update_cond;
atomic<bool> update_exit;

void update(auto& ipapi)
{
    while (!update_exit.load()) {
        // Waiting for next update
        uint64_t now = chrono::duration_cast<chrono::seconds>(
            chrono::system_clock::now().time_since_epoch()).count();
        int32_t timeout = ipapi.LastUpdateSuccessful() ?
            ipapi.UpdateTimeout() : ipapi.RetryTimeout();
        uint64_t next_update = ipapi.LastUpdate() + timeout;
        if (now < next_update) {
            uint32_t sleep = next_update - now;
            clog << "Next update in " << sleep << " seconds" << endl;
            mutex mtx;
            unique_lock<mutex> lck(mtx);
            update_cond.wait_for(lck, chrono::seconds(sleep));
            continue;
        }

        clog << "Updating Ipapi values" << endl;
        ipapi.LastUpdate = now;
        bool last_update_successful = true;
        last_update_successful &= ipapi.update_values();
        ipapi.LastUpdateSuccessful = last_update_successful;

        ipapi.properties_changed({"LastUpdate", "LastUpdateSuccessful"});
    }
    clog << "Exiting Ipapi update thread" << endl;
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

void signal_handler(int sig)
{
    clog << strsignal(sig) << " signal received" << endl;
    dispatcher.leave();
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used for Dbus object. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, IpapiName, nullptr,
            "The Dbus name of the Ipapi"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, IpapiPath, nullptr,
            "The Dbus path of the Ipapi"
        },
    });

    // Setup signal handler
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    clog << "Setting up Ipapi proxy" << endl;
    IpapiAdaptor ipapi(conn);
    ipapi.RetryTimeout = 60;
    ipapi.UpdateTimeout = 60*60*3;
    ipapi.LastUpdate = 0;
    ipapi.LastUpdateSuccessful = false;

    ipapi.onSet = [&ipapi](auto& iface, auto& name, auto& value) {
        if (name == "RetryTimeout") {
            ipapi.RetryTimeout = value.operator uint32_t();
            if (!ipapi.LastUpdateSuccessful())
                update_cond.notify_one();
            ipapi.properties_changed({name});
        }
        if (name == "UpdateTimeout") {
            ipapi.UpdateTimeout = value.operator uint32_t();
            if (ipapi.LastUpdateSuccessful())
                update_cond.notify_one();
            ipapi.properties_changed({name});
        }
    };

    clog << "Starting update thread" << endl;
    thread update_thread([&ipapi]() { update(ipapi); });

    clog << "Requesting DBus name" << endl;
    conn.request_name(bus_name.c_str());

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    clog << "Waiting update thread to exit" << endl;
    update_exit.store(true);
    update_cond.notify_one();
    update_thread.join();

    return 0;
}