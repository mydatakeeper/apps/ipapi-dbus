#include "ipapi-adaptor.h"

#include <mydatakeeper/download.h>
#include <json/json.hh>

const string IpapiUrl("https://ipapi.co/json");

namespace {

template <typename T>
inline DBus::Variant to_new_variant(const DBus::Variant& v1, const T t2)
{
    T t1 = v1.operator T();
    if (t1 != t2)
        return to_variant(t2);
    return {};
}

DBus::Variant to_new_variant(const DBus::Variant& v1, const JSON::Value& v2)
{
    auto sig = v1.signature();
    auto type = v2.type();
    if (sig == "b" && type == JSON::BOOL)
        return to_new_variant<bool>(v1, v2.as_bool());
    if (sig == "n" && type == JSON::INT)
        return to_new_variant<int16_t>(v1, v2.as_int());
    if (sig == "q" && type == JSON::INT)
        return to_new_variant<uint16_t>(v1, v2.as_int());
    if (sig == "i" && type == JSON::INT)
        return to_new_variant<int32_t>(v1, v2.as_int());
    if (sig == "u" && type == JSON::INT)
        return to_new_variant<uint32_t>(v1, v2.as_int());
    if (sig == "x" && type == JSON::INT)
        return to_new_variant<int64_t>(v1, v2.as_int());
    if (sig == "t" && type == JSON::INT)
        return to_new_variant<uint64_t>(v1, v2.as_int());
    if (sig == "d" && type == JSON::FLOAT)
        return to_new_variant<double>(v1, v2.as_float());
    if (sig == "s" && type == JSON::STRING)
        return to_new_variant<string>(v1, v2.as_string());
    return {};
}

}

extern const string IpapiPath("/co/ipapi");

IpapiAdaptor::IpapiAdaptor(DBus::Connection &connection)
: DBusAdaptor(connection, IpapiPath)
{
    this->ip = string();
    this->city = string();
    this->region = string();
    this->region_code = string();
    this->country = string();
    this->country_name = string();
    this->continent_code = string();
    this->in_eu = false;
    this->postal = string();
    this->latitude = 0.0;
    this->longitude = 0.0;
    this->timezone = string();
    this->utc_offset = string();
    this->country_calling_code = string();
    this->currency = string();
    this->languages = string();
    this->asn = string();
    this->org = string();
}

void IpapiAdaptor::properties_changed(const vector<string>& properties)
{
    map<string, DBus::Variant> changed;
    for(auto& prop : properties)
        changed[prop] = *co::ipapi_adaptor::get_property(prop);
    PropertiesChanged(co::ipapi_adaptor::name(), changed, {});
}

bool IpapiAdaptor::update_values()
{
    string buffer, err;
    if (!download_file(IpapiUrl, buffer, &err)) {
        cerr << err << endl;
        return false;
    }

    const vector<string> defined_values = {
        "ip", "city", "region", "region_code", "country", "country_name",
        "continent_code", "in_eu", "postal", "latitude", "longitude",
        "timezone", "utc_offset", "country_calling_code", "currency",
        "languages", "asn", "org",
    };

    JSON::Object json = parse_string(buffer);
    map<string, DBus::Variant> updated_values;
    for (auto& it : json) {
        auto search = find(defined_values.begin(), defined_values.end(), it.first);
        if (search == defined_values.end())
            continue;
        DBus::Variant *old_value = co::ipapi_adaptor::get_property(it.first);
        DBus::Variant new_value = to_new_variant(*old_value, it.second);
        if (!new_value.signature().empty()) {
            clog << "Updating value \"" << it.first << "\" to " << it.second << endl;
            auto& property = co::ipapi_adaptor::_properties[it.first].value;
            property = new_value;
            updated_values[it.first] = new_value;
        }
    }

    PropertiesChanged(co::ipapi_adaptor::name(), updated_values, {});

    return true;
}
