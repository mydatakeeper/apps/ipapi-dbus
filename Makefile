# Makefile
.SUFFIXES:

CXX:=$(CROSS_COMPILE)g++
EXTRA_CFLAGS:=-std=c++17 -I./include -fconcepts $(shell pkg-config mydatakeeper --cflags)
EXTRA_LDFLAGS:=$(shell pkg-config mydatakeeper --libs) -ljson -lpthread

TARGET=ipapi-dbus
OBJ_FILES=ipapi-dbus.o ipapi-adaptor.o
INC_FILES=ipapi-adaptor.h
GEN_FILES=co.ipapi.h

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) $(EXTRA_LDFLAGS)

%.h:%.xml
	dbusxx-xml2cpp $< --adaptor=$@

%.o:%.cpp $(INC_FILES) $(GEN_FILES)
	$(CXX) -c $< -o $@ $(CFLAGS) $(EXTRA_CFLAGS)

clean:
	rm -rf $(TARGET) $(OBJ_FILES) $(GEN_FILES)